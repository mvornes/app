require 'rails_helper'

RSpec.describe User, type: :model do
    before(:each) do
        @t = FactoryGirl.create :user
        @r = FactoryGirl.create :user_interpreter
        @to = FactoryGirl.create :user_reviewer
        @rto = FactoryGirl.create :user_interpreter_and_reviewer
    end

    it 'should check it a user as role' do
        expect(@t.roles).to be_empty
        expect(@r.has_role?(:interpreter)).to be true
        expect(@rto.has_role?(:interpreter, :reviewer)).to be true
    end

    it 'should add roles just once' do
        role = Role.find_by identifier: 'reviewer'
        expect { @to.roles << role }.to raise_error(ActiveRecord::RecordNotUnique)
        expect(@to.roles.count).to eq(1)
    end

    it 'should update password when it is present' do
      params = {name: 'Linus', password: '123456'}
      @t.update_with_or_without_pwd(params)

      expect(@t.name).to eq('Linus')
      expect(@t.password).to eq('123456')
    end

    it 'should update other attributes if password is not present' do
      params = {name: 'Linus', password: ''}
      @t.update_with_or_without_pwd(params)

      expect(@t.name).to eq('Linus')
    end

    it 'should update other attributes if password is not present' do
      params = {name: 'Linus', password: ''}
      @t.update_with_or_without_pwd(params)

      expect(@t.name).to eq('Linus')
    end



end
