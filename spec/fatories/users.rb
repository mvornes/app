FactoryGirl.define do
  factory :user do
    name 'Michael Vornes'
    sequence :email { |n| "tcc#{n}@tsi.gp.utfpr.edu.br" }
    password               "password"
    password_confirmation  "password"
  end
end
