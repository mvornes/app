require 'rails_helper'

RSpec.describe Users::DashboardController, type: :controller do

  describe "GET #index" do
    it "returns http 302 without user login" do
      get :index
      expect(response).to have_http_status(302)
    end

     it "returns http success" do
      user = FactoryGirl.create(:user)
      sign_in(user, scope: :user)

      get :index
      expect(response).to have_http_status(:success)
    end
  end

end
