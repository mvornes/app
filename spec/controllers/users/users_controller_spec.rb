require 'rails_helper'

RSpec.describe Users::UsersController, type: :controller do

  describe "GET #destroy" do

    it "should not remove it self" do
      user = FactoryGirl.create(:user)
      sign_in(user, scope: :user)

      delete :destroy, params: {id: user.id}

      expect(User.count).to eq(1)
    end

  end

end
