require 'rails_helper'

describe ApplicationHelper do
  describe "#page_title" do
    it "full title helpe" do
      expect(helper.full_title).to eq("SGTCC")
      expect(helper.full_title('Home')).to eq("Home | SGTCC")
    end
  end
end
