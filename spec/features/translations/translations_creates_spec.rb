require 'rails_helper'

RSpec.feature "Translations::Creates", type: :feature do

  it "displays the success message after create translation" do
      user = FactoryGirl.create(:user, email: 'user@test.com')

      visit new_translation_path
      fill_in 'translation_subject', with: 'titulo'
      fill_in 'translation_description', with: 'resumo'

      find('input[name="commit"]').click # click_in 'Entrar'

      expect(page.current_path).to eq translations_path
      expect(page).to have_selector('div.alert.alert-info',
                                    text: 'Tradução cadastrada com sucesso')
    end

end
