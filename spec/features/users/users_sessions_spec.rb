require 'rails_helper'

describe "Users:Sessions", type: :feature do

    it "displays the user's perfil links after successful login" do
        user = FactoryGirl.create(:user, email: 'tcc@gp.utfpr.edu.br')

        visit new_user_session_path
        fill_in 'user_email', with: user.email
        fill_in 'user_password', with: user.password

        find('input[name="commit"]').click # click_in 'Entrar'

        expect(page.current_path).to eq users_root_path
        expect(page).to have_selector('div.alert.alert-info',
        text: 'Login efetuado com sucesso!')
    end

    it "displays the user's error message when user or password is wrong" do
        user = FactoryGirl.create(:user, email: 'tcc@gp.utfpr.edu.br')

        visit new_user_session_path
        fill_in 'user_email', with: 'tcc@gp.utfpr.edu.br'
        fill_in 'user_password', with: 'passworda'

        find('input[name="commit"]').click # click_in 'Entrar'

        expect(page.current_path).to eq new_user_session_path
        expect(page).to have_selector('div.alert.alert-warning',
        text: 'E-mail ou senha inválidos.')
    end

    it 'displays success logout message when the user click on logout' do
        user = FactoryGirl.create(:user)
        login_as(user, scope: :user)

        visit users_root_url
        expect(page).to have_link('Meu perfil', href: edit_users_user_path(user))
        expect(page).to have_link('Sair', href: destroy_user_session_path)

        click_link('Sair')
        expect(page.current_path).to eq new_user_session_path
        expect(page).to have_selector('div.alert.alert-info',
        text: 'Saiu com sucesso.')
    end

    it 'should be possible to reset its password' do
        user = FactoryGirl.create(:user)
        visit new_user_session_path

        click_link 'Esqueceu a senha?'
        fill_in 'user_email', with: user.email

        expect do
            find('input[name="commit"]').click
        end.to change(ActionMailer::Base.deliveries, :count).by(1)

        token = user.send(:set_reset_password_token)
        visit edit_user_password_path(reset_password_token: token)

        fill_in 'user_password', with: 'Passw0rd!'
        fill_in 'user_password_confirmation', with: 'Passw0rd!'
        find('input[name="commit"]').click

        expect(page).to have_selector('div.alert.alert-info',
        text: 'Sua senha foi alterada com sucesso. Você está logado.')
    end
end
