# README

#### PHEW! Traduções Colaborativas

PHEW! Traduções Colaborativas é um sistema para traduções colaborativas. No sistema o usuário pode cadastrar um pedido de tradução, com arquivo que deseja traduzir e demais informações, então este pedido fica disponível para que outros usuários façam sugestões de tradução e postem arquivos respostas. O usuário pode visualizar os seus pedidos de traduções postados, bem como, as respostas que fez em pedidos de traduções de outros usuários.

Michael Vornes - Web 5 Project - UTFPR
