class RemoveIdentifierFromResponses < ActiveRecord::Migration[5.0]
  def change
    remove_column :responses, :identifier, :string
  end
end
