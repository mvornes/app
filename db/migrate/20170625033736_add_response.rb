class AddResponse < ActiveRecord::Migration[5.0]
  def change

    create_table :responses do |t|
      t.string :message, null: false
      t.string :identifier, unique: true, index: true
      t.timestamps
    end

    create_table :responses_translations, id: false do |t|
      t.belongs_to :translation, index: true
      t.belongs_to :response, index: true
    end

    add_index :responses_translations, [:response_id, :translation_id], unique: true

  end
end
