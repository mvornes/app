class AddAttachmentSourcefileToTranslations < ActiveRecord::Migration
  def self.up
    change_table :translations do |t|
      t.attachment :sourcefile
    end
  end

  def self.down
    remove_attachment :translations, :sourcefile
  end
end
