class CreateTranslations < ActiveRecord::Migration[5.0]
  def change
    create_table :translations do |t|
      t.string :subject
      t.string :description

      t.timestamps
    end
  end
end
