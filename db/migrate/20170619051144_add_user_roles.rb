class AddUserRoles < ActiveRecord::Migration[5.0]
  def change
    create_table :roles do |t|
      t.string :name, null: false
      t.string :identifier, unique: true, index: true
      t.timestamps
    end

    create_table :roles_users, id: false do |t|
      t.belongs_to :user, index: true
      t.belongs_to :role, index: true
    end

    add_index :roles_users, [:role_id, :user_id], unique: true
  end
end
