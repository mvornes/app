class AddTranslationIdToResponses < ActiveRecord::Migration[5.0]
  def change
    add_column :responses, :translation_id, :integer
  end
end
