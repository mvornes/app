class AddAttachmentSourcefileToResponses < ActiveRecord::Migration[5.0]
  def self.up
    change_table :responses do |t|
      t.attachment :sourcefile
    end
  end

  def self.down
    remove_attachment :responses, :sourcefile
  end
end
