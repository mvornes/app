class AddCustomerIdToTranslations < ActiveRecord::Migration[5.0]
  def change
    add_column :translations, :customer_id, :integer
  end
end
