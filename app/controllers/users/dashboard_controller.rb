class Users::DashboardController < Users::AppUserController
  def index
    @users = User.all
    @translations = Translation.all
  end
end
