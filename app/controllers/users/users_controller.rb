class Users::UsersController < Users::AppUserController

  def index
    @users = User.all
    @translations = Translation.all
  end

  def new
    @user = User.new
  end

  def show
    @user = User.find(params[:id])
  end


  def create
    @user = User.new user_params

    if @user.save
      flash[:success] = "Usuário #{@user.name} criado com sucesso"
      redirect_to users_users_path
    else
      flash.now[:error] = "Existem dados incorretos no formulário"
      render :new
    end
  end

  def edit
    @user = User.find(params[:id])
  end

  def update
    @user = User.find(params[:id])

    if @user.update_with_or_without_pwd user_params
      flash[:success] = "Usuário #{@user.name} atualizado com sucesso"
      redirect_to users_users_path
    else
      flash[:error] = "Existem dados incorretos no formulário"
      render :edit
    end
  end

  def destroy
    @user = User.find(params[:id])
    @user.destroy
    flash[:notice] = "Usuário #{@user.name} removido com sucesso"
    redirect_to users_users_path
  end


  def destroy
    @user = User.find(params[:id])

    if current_user == @user
      flash[:error] = "Você não pode se autoremover"
    else
      @user.destroy
      flash[:notice] = "Usuário #{@user.name} removido com sucesso"
    end

    redirect_to users_users_path
  end

private
  def user_params
    params.require(:user)
    .permit(:name, :email, :password, :password_confirmation, {role_ids: []})
  end


end
