class TranslationsController < ApplicationController

  def index
  	@translations = Translation.all
  end

  def new
    @translation = Translation.new
  end

  def show
    @translation = Translation.find(params[:id])
    @responses = Response.all
    @response = Response.new
    @response.translation = @translation
  end

  def create
    @translation = Translation.new (translation_params)
    @translation.user = current_user

    if @translation.save
      flash[:success] = "Tradução cadastrada com sucesso"
      redirect_to translations_path
    else
      flash.now[:error] = "Existem dados incorretos no formulário"
      render :new
    end
  end

  def edit
     @translation = Translation.find(params[:id])
  end

  def update
    @translation = Translation.find(params[:id])

    if @translation.update translation_params
      flash[:success] = "Tradução atualizada com sucesso"
    else
      flash[:error] = "Existem dados incorretos no formulário"
      render :edit
    end
  end

  def destroy
    @translation = Translation.find(params[:id])
    @translation.destroy
    flash[:notice] = "Tradução removida com sucesso"
    redirect_to translations_path
  end

private
  def translation_params
    params.require(:translation)
    .permit(:subject, :description, :sourcefile)
  end



end
