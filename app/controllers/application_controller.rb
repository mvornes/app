class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  layout :layout_by_resource

  def after_sign_in_path_for(resource)
    return users_root_path if resource.is_a? User
    return customers_root_path if resource.is_a? Customer
  end

  def after_sign_out_path_for(resource)
    return new_user_session_path if resource == :user
    return new_customer_session_path if resource == :customer
  end

  protected
  def layout_by_resource
    return "layouts/session" if devise_controller?

    "layouts/application"
  end
end
