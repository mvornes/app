class ResponsesController < ApplicationController
  def index
  	@responses = Response.all
    @translations = Translation.all
  end

  def new
      @response = Response.new
  end

  def show
    	@response = Response.find(params[:id])
  end

  def create

    @response = Response.new response_params
    @response.user = current_user

    if @response.save
      flash[:success] = "Resposta cadastrada com sucesso"
      redirect_to :back
    else
      flash.now[:error] = "Existem dados incorretos no formulário"
      redirect_to :back
    end
  end

  def edit
     @response = Response.find(params[:id])
  end

  def update
    @response = Response.find(params[:id])

    if @response.update response_params
      flash[:success] = "Resposta atualizada com sucesso"
    else
      flash[:error] = "Existem dados incorretos no formulário"
      render :edit
    end
  end

  def destroy
    @response = Response.find(params[:id])
    @response.destroy
    flash[:notice] = "Resposta removida com sucesso"
    redirect_to :back
  end

private
  def response_params
    params.require(:response)
    .permit(:message, :sourcefile, :translation_id)
  end

end
