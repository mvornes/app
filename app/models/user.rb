class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable,
         :recoverable, :rememberable, :trackable, :validatable

  has_and_belongs_to_many :roles
  has_many :translations
  has_many :responses

  def has_role?(*roles)
     !self.roles.where(identifier: roles).empty?
  end

  def update_with_or_without_pwd(params)
    update_method = :update_without_password
    unless params[:password].blank?
      update_method = :update
    end

    send(update_method, params)
  end

private
  def ensure_that_exist_at_one_user_as_interpreter
    rr = Role.find_by(identifier: :interpreter)

    if !new_record? && rr && rr.user_ids.empty?
      errors.add(:roles,
      "Deve existir ao menos um usuário com o papel de 'Tradutor'")
      return false
    else
      return true
    end
  end

end
