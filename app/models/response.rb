class Response < ApplicationRecord

  has_attached_file :sourcefile
  validates_attachment_size :sourcefile, :less_than => 2.megabytes
  validates_attachment_content_type :sourcefile, :content_type => ['text/plain', 'application/pdf', 'application/msword' ]

  belongs_to :user
  belongs_to :translation
end
