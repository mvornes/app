set :stage, :staging

server '159.203.177.218', roles: %w(app web db), primary: true, user: 'deployer'
set :rails_env, "production"
