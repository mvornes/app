Rails.application.routes.draw do

  root to: 'home#index'

  devise_for :users
  namespace :users do
    root to: 'dashboard#index'
    resources :users
  end

  resources :translations
  resources :responses


  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'



  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
